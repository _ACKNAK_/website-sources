# Description
Website sources from: https://acknak.fr/ 

Customized version based on:

\_ GoHugo

\_ Hyde-Hyde's theme

\_ A fork from Laluka (https://github.com/ThinkLoveShare/sources)

\_ Some customization from Haax (https://github.com/Haaxmax/haaxmax.github.io)


I'm sharing the sources used to generate the Website, since it took me some times to adjust to my needs.